# hotel-filter



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type                                                         | Default     |
| -------- | --------- | ----------- | ------------------------------------------------------------ | ----------- |
| `active` | `active`  |             | `boolean`                                                    | `false`     |
| `icon`   | `icon`    |             | `string`                                                     | `undefined` |
| `text`   | `text`    |             | `SortEnum.alphabetically \| SortEnum.price \| SortEnum.star` | `undefined` |


## Events

| Event  | Description | Type               |
| ------ | ----------- | ------------------ |
| `sort` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [hotel-sort-container](../../containers/sort-container)

### Graph
```mermaid
graph TD;
  hotel-sort-container --> hotel-filter
  style hotel-filter fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
