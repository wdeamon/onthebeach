import {
  Component,
  Host,
  h,
  Listen,
  Event,
  EventEmitter,
  Prop
} from '@stencil/core';
import { SortEnum } from '../../enums/SortEnum';

@Component({
  tag: 'hotel-filter',
  styleUrl: 'filter.css',
  shadow: true
})
export class Filter {
  @Prop() text: SortEnum;
  @Prop() icon: string;
  @Prop() active: boolean = false;

  @Event({ bubbles: true }) sort: EventEmitter;

  @Listen('click')
  handleClick() {
    this.sort.emit({ name: this.text, active: !this.active });
  }

  render() {
    return (
      <Host>
        <div class={`filter ${this.active ? 'active' : ''}`}>
          <div>
            Sort {this.text === SortEnum.alphabetically ? '' : 'by'}{' '}
            <span class="important">{this.text}</span>
          </div>
          <div class="icon">{this.icon}</div>
        </div>
      </Host>
    );
  }
}
