import { Filter } from './filter';
import { newSpecPage } from '@stencil/core/dist/testing';

describe('hotel-filter', () => {
  it('should match snapshot', async () => {
    const { root } = await newSpecPage({
      components: [Filter],
      html: '<hotel-filter></hotel-filter>'
    });
    expect(root).toMatchSnapshot();
  });
});
