import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'card-overview',
  styleUrl: 'overview.css',
  shadow: true
})
export class Overview {
  @Prop() text: string;

  render() {
    return (
      <Host>
        <div class="overview-container">
          <div class="title">Overview</div>
          <div class="text">{this.text}</div>
        </div>
      </Host>
    );
  }
}
