import { Overview } from './overview';
import { newSpecPage } from '@stencil/core/dist/testing';

describe('card-overview', () => {
  it('should match snapshot', async () => {
    const { root } = await newSpecPage({
      components: [Overview],
      html: `<card-overview text="this is my overview text"></card-overview>`
    });

    expect(root).toMatchSnapshot();
  });
});
