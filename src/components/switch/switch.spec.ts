import { Switch } from './switch';
import { newSpecPage } from '@stencil/core/dist/testing';

describe('overview-switch', () => {
  it('should match snapshot', async () => {
    const { root } = await newSpecPage({
      components: [Switch],
      html: '<overview-switch></overview-switch>'
    });
    expect(root).toMatchSnapshot();
  });
});
