import {
  Component,
  Host,
  h,
  State,
  Listen,
  EventEmitter,
  Event
} from '@stencil/core';

@Component({
  tag: 'overview-switch',
  styleUrl: 'switch.css',
  shadow: true
})
export class Switch {
  @State() open: boolean = false;

  @Event() switched: EventEmitter;

  @Listen('click', { capture: true })
  handleClick() {
    this.open = !this.open;
    this.switched.emit(this.open);
  }

  getButtonText() {
    return this.open
      ? 'Read less about this hotel'
      : 'Read more about this motel';
  }

  getArrowRight() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
      >
        <path d="M0 7.33l2.829-2.83 9.175 9.339 9.167-9.339 2.829 2.83-11.996 12.17z" />
      </svg>
    );
  }

  getArrowDown() {
    return (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="24"
        height="24"
        viewBox="0 0 24 24"
      >
        <path d="M5 3l3.057-3 11.943 12-11.943 12-3.057-3 9-9z" />
      </svg>
    );
  }

  render() {
    return (
      <Host>
        <div class="button-container">
          <div>
            {this.open ? <span>Read less</span> : <span>Read more</span>} about
            this hotel
          </div>
          <div>{!this.open ? this.getArrowDown() : this.getArrowRight()}</div>
        </div>
      </Host>
    );
  }
}
