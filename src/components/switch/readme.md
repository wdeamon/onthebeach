# overview-switch



<!-- Auto Generated Below -->


## Events

| Event      | Description | Type               |
| ---------- | ----------- | ------------------ |
| `switched` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [hotel-card](../../containers/card)

### Graph
```mermaid
graph TD;
  hotel-card --> overview-switch
  style overview-switch fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
