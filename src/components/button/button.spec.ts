import { Button } from './button';
import { newSpecPage } from '@stencil/core/testing';

describe('submit-button', () => {
  it('should render my component', async () => {
    const page = await newSpecPage({
      components: [Button],
      html: `<submit-button text="Book now" price="£123.45" />`
    });

    expect(page.root).toMatchSnapshot();
  });
});
