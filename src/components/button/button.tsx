import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'submit-button',
  styleUrl: 'button.css',
  shadow: true
})
export class Button {
  @Prop()
  callback;

  @Prop()
  text: string;

  @Prop()
  price: string;

  render() {
    return (
      <Host>
        <div class="submit-button" onClick={this.callback}>
          <div class="text">{this.text}</div>
          <div class="price">{this.price}</div>
        </div>
      </Host>
    );
  }
}
