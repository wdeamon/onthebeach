# submit-button



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute  | Description | Type     | Default     |
| ---------- | ---------- | ----------- | -------- | ----------- |
| `callback` | `callback` |             | `any`    | `undefined` |
| `price`    | `price`    |             | `string` | `undefined` |
| `text`     | `text`     |             | `string` | `undefined` |


## Dependencies

### Used by

 - [hotel-card](../../containers/card)

### Graph
```mermaid
graph TD;
  hotel-card --> submit-button
  style submit-button fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
