# hotel-star



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute         | Description | Type     | Default |
| --------------- | ----------------- | ----------- | -------- | ------- |
| `numberOfStars` | `number-of-stars` |             | `number` | `1`     |


## Dependencies

### Used by

 - [hotel-card](../../containers/card)

### Graph
```mermaid
graph TD;
  hotel-card --> hotel-star
  style hotel-star fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
