import { Star } from './star';
import { newSpecPage } from '@stencil/core/dist/testing';

describe('hotel-star', () => {
  it('should match snapshot', async () => {
    const { root } = await newSpecPage({
      components: [Star],
      html: `<hotel-star numberOfStars=${3}></hotel-star>`
    });
    expect(root).toMatchSnapshot();
  });
});
