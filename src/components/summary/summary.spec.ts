import { Summary } from './summary';
import { newSpecPage } from '@stencil/core/dist/testing';

describe('card-summary', () => {
  it('should render component', async () => {
    const noOfUsers = {
      adults: 1,
      children: 10,
      infants: 5
    };
    const date = '2019-12-10T23:23:23.996';
    const noOfDays = 7;
    const departuringFrom = 'Manchester';

    const { root } = await newSpecPage({
      components: [Summary],
      html: `<card-summary noOfUsers=${noOfUsers} date=${date} noOfDays=${noOfDays} departingFrom=${departuringFrom} />`
    });

    expect(root).toMatchSnapshot();
  });
});
