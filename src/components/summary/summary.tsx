import { Component, Host, h, Prop } from '@stencil/core';
import { format } from 'date-fns';

export interface NumberOfUsers {
  adults: number;
  children: number;
  infants: number;
}

@Component({
  tag: 'card-summary',
  styleUrl: 'summary.css',
  shadow: true
})
export class Summary {
  @Prop()
  noOfUsers: NumberOfUsers = { adults: 0, children: 0, infants: 0 };

  @Prop()
  date: string;

  @Prop()
  noOfDays: number;

  @Prop()
  departingFrom: string;

  getNumberOfUsers(): HTMLElement {
    return (
      <span>
        <span class="important">{this.noOfUsers.adults}</span> adult
        {this.noOfUsers.adults === 1 ? '' : 's'},{' '}
        <span class="important">{this.noOfUsers.children}</span>{' '}
        {this.noOfUsers.children === 1 ? 'child' : 'children'} &amp;{' '}
        <span class="important">{this.noOfUsers.infants}</span> infant
        {this.noOfUsers.infants === 1 ? '' : 's'}
      </span>
    );
  }

  getDate(): string {
    return (
      <span>
        <span class="important">
          {format(new Date(this.date), 'io MMMM yyyy')}
        </span>{' '}
        for <span class="important">{this.noOfDays}</span> day
        {this.noOfDays === 1 ? '' : 's'}
      </span>
    );
  }

  render() {
    return (
      <Host>
        <div>
          <div>{this.getNumberOfUsers()}</div>
          <div>{this.getDate()}</div>
          <div>
            Departing from <span class="important">{this.departingFrom}</span>
          </div>
        </div>
      </Host>
    );
  }
}
