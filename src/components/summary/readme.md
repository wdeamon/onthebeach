# card-summary



<!-- Auto Generated Below -->


## Properties

| Property        | Attribute        | Description | Type            | Default                                  |
| --------------- | ---------------- | ----------- | --------------- | ---------------------------------------- |
| `date`          | `date`           |             | `string`        | `undefined`                              |
| `departingFrom` | `departing-from` |             | `string`        | `undefined`                              |
| `noOfDays`      | `no-of-days`     |             | `number`        | `undefined`                              |
| `noOfUsers`     | --               |             | `NumberOfUsers` | `{ adults: 0, children: 0, infants: 0 }` |


## Dependencies

### Used by

 - [hotel-card](../../containers/card)

### Graph
```mermaid
graph TD;
  hotel-card --> card-summary
  style card-summary fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
