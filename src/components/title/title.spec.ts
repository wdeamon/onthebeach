import { Title } from './title';
import { newSpecPage } from '@stencil/core/dist/testing';

describe('card-title', () => {
  it('should render component', async () => {
    const { root } = await newSpecPage({
      components: [Title],
      html:
        '<card-title name="Hotel one" location="Manchester, United Kingdom" />'
    });

    expect(root).toMatchSnapshot();
  });
});
