import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'card-title',
  styleUrl: 'title.css',
  shadow: true
})
export class Title {
  @Prop() name: string;
  @Prop() location: string;

  render() {
    return (
      <Host>
        <div class="card-title">
          <div class="name">{this.name}</div>
          <div class="location">{this.location}</div>
        </div>
      </Host>
    );
  }
}
