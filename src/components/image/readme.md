# hotel-image



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default     |
| -------- | --------- | ----------- | -------- | ----------- |
| `alt`    | `alt`     |             | `string` | `undefined` |
| `name`   | `name`    |             | `string` | `undefined` |


## Dependencies

### Used by

 - [hotel-card](../../containers/card)

### Graph
```mermaid
graph TD;
  hotel-card --> hotel-image
  style hotel-image fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
