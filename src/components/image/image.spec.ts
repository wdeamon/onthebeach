import { Image } from './image';
import { newSpecPage } from '@stencil/core/dist/testing';

describe('hotel-image', () => {
  it('should match snapshot', async () => {
    const { root } = await newSpecPage({
      components: [Image],
      html: `<hotel-image name="hotel-image-1.png" alt="hotel 1"></hotel-image>`
    });
    expect(root).toMatchSnapshot();
  });
});
