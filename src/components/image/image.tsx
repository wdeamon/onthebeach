import { Component, Host, h, Prop } from '@stencil/core';
import { assetsUrl } from '../../config/default';

@Component({
  tag: 'hotel-image',
  styleUrl: 'image.css',
  shadow: true
})
export class Image {
  @Prop()
  name: string;

  @Prop()
  alt: string;

  render() {
    return (
      <Host>
        <img src={`${assetsUrl}${this.name}`} alt={this.alt}></img>
      </Host>
    );
  }
}
