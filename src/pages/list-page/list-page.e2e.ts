import { newE2EPage } from '@stencil/core/testing';

describe('hotel-list-page', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<hotel-list-page></hotel-list-page>');

    const element = await page.find('hotel-list-page');
    expect(element).toHaveClass('hydrated');
  });
});
