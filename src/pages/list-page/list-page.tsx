import { Component, Host, h, Listen, State } from '@stencil/core';
import Hotels from '../../stubs/hotels.json';
import { SortEnum } from '../../enums/SortEnum';

@Component({
  tag: 'hotel-list-page',
  styleUrl: 'list-page.css',
  shadow: true
})
export class ListPage {
  @State() displayData = Hotels;
  @State() activeSort = SortEnum.price;

  @Listen('sortBy')
  handleSort(event) {
    if (event.detail) {
      this.activeSort = event.detail;
      this.sortData();
    } else {
      this.displayData = Hotels;
    }
  }

  componentDidLoad() {
    console.log('component did load');
    this.sortData();
  }

  sortData() {
    this.displayData = [...this.displayData].sort((a, b) => {
      switch (this.activeSort) {
        case SortEnum.alphabetically:
          let sortA = a.name.toUpperCase();
          let sortB = b.name.toUpperCase();
          return sortA < sortB ? -1 : sortA > sortB ? 1 : 0;
        case SortEnum.price:
          return b.price - a.price;
        case SortEnum.star:
          return b.stars - a.stars;
      }
    });
  }

  getCards = data => {
    const cards = [];
    data.forEach(hotel => {
      cards.push(
        <div class="card-container">
          <hotel-card cardOptions={hotel}></hotel-card>
        </div>
      );
    });
    return cards;
  };

  render() {
    return (
      <Host>
        <div class="page-container">
          <div class="filter-container">
            <hotel-sort-container></hotel-sort-container>
          </div>
          <div class="card-container">
            {this.displayData && this.getCards(this.displayData)}
          </div>
        </div>
      </Host>
    );
  }
}
