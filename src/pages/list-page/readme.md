# hotel-list-page



<!-- Auto Generated Below -->


## Dependencies

### Depends on

- [hotel-card](../../containers/card)
- [hotel-sort-container](../../containers/sort-container)

### Graph
```mermaid
graph TD;
  hotel-list-page --> hotel-card
  hotel-list-page --> hotel-sort-container
  hotel-card --> hotel-image
  hotel-card --> overview-switch
  hotel-card --> card-title
  hotel-card --> hotel-star
  hotel-card --> card-summary
  hotel-card --> submit-button
  hotel-card --> card-overview
  hotel-sort-container --> hotel-filter
  style hotel-list-page fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
