import { Component, Host, h, State, Listen, Prop } from '@stencil/core';

export interface CardOptions {
  name: string;
  location: string;
  image: {
    name: string;
    alt: string;
  };
  stars: number;
  numberOfDays: number;
  date: string;
  departingFrom: string;
  numberOfUsers: { adults: number; children: number; infants: number };
  price: number;
  currency: string;
  overview: string;
}

@Component({
  tag: 'hotel-card',
  styleUrl: 'card.css',
  shadow: true
})
export class Card {
  @Prop() cardOptions: CardOptions;

  submitCallback = () => {
    console.log('Submit button clicked!');
  };

  @State() switched: boolean = false;

  @Listen('switched')
  handleSwitched() {
    this.switched = !this.switched;
  }

  render() {
    return (
      this.cardOptions && (
        <Host>
          <div class="card">
            <div class="card-content">
              <div class="content">
                <div class="image-container">
                  <div class="image">
                    <hotel-image
                      name={this.cardOptions.image.name}
                      alt={this.cardOptions.image.alt}
                    ></hotel-image>
                  </div>
                  <div class="button">
                    <overview-switch></overview-switch>
                  </div>
                </div>
              </div>
              <div class="content">
                <div>
                  <card-title
                    name={this.cardOptions.name}
                    location={this.cardOptions.location}
                  ></card-title>
                </div>
                <div class="star-container">
                  <hotel-star
                    numberOfStars={this.cardOptions.stars}
                  ></hotel-star>
                </div>
                <div>
                  <card-summary
                    noOfDays={this.cardOptions.numberOfDays}
                    date={this.cardOptions.date}
                    departingFrom={this.cardOptions.departingFrom}
                    noOfUsers={this.cardOptions.numberOfUsers}
                  ></card-summary>
                </div>
                <div>
                  <submit-button
                    text="Book now"
                    price={`${this.cardOptions.currency}${this.cardOptions.price}`}
                    callback={this.submitCallback}
                  ></submit-button>
                </div>
              </div>
            </div>
            <div class="card-content">
              {this.switched && (
                <card-overview text={this.cardOptions.overview}></card-overview>
              )}
            </div>
          </div>
        </Host>
      )
    );
  }
}
