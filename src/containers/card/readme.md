# hotel-card



<!-- Auto Generated Below -->


## Properties

| Property      | Attribute | Description | Type          | Default     |
| ------------- | --------- | ----------- | ------------- | ----------- |
| `cardOptions` | --        |             | `CardOptions` | `undefined` |


## Dependencies

### Used by

 - [hotel-list-page](../../pages/list-page)

### Depends on

- [hotel-image](../../components/image)
- [overview-switch](../../components/switch)
- [card-title](../../components/title)
- [hotel-star](../../components/star)
- [card-summary](../../components/summary)
- [submit-button](../../components/button)
- [card-overview](../../components/overview)

### Graph
```mermaid
graph TD;
  hotel-card --> hotel-image
  hotel-card --> overview-switch
  hotel-card --> card-title
  hotel-card --> hotel-star
  hotel-card --> card-summary
  hotel-card --> submit-button
  hotel-card --> card-overview
  hotel-list-page --> hotel-card
  style hotel-card fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
