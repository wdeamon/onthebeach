import { Card } from './card';
import { newSpecPage } from '@stencil/core/dist/testing';
import Hotels from '../../stubs/hotels.json';

describe('hotel-card', () => {
  it('should match snapshot', async () => {
    const { root, rootInstance, waitForChanges } = await newSpecPage({
      components: [Card],
      html: `<hotel-card></hotel-card>`
    });

    rootInstance.cardOptions = Hotels[0];
    await waitForChanges();

    expect(root).toMatchSnapshot();
  });
});
