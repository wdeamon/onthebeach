# hotel-sort-container



<!-- Auto Generated Below -->


## Events

| Event    | Description | Type               |
| -------- | ----------- | ------------------ |
| `sortBy` |             | `CustomEvent<any>` |


## Dependencies

### Used by

 - [hotel-list-page](../../pages/list-page)

### Depends on

- [hotel-filter](../../components/filter)

### Graph
```mermaid
graph TD;
  hotel-sort-container --> hotel-filter
  hotel-list-page --> hotel-sort-container
  style hotel-sort-container fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
