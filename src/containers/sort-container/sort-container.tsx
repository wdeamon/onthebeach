import {
  Component,
  Host,
  h,
  Listen,
  State,
  Event,
  EventEmitter
} from '@stencil/core';
import { SortEnum } from '../../enums/SortEnum';

@Component({
  tag: 'hotel-sort-container',
  styleUrl: 'sort-container.css',
  shadow: true
})
export class SortContainer {
  @State() activeSort: string = SortEnum.price;

  @Event() sortBy: EventEmitter;

  @Listen('sort')
  handleSort(ev) {
    this.activeSort = ev.detail.active ? ev.detail.name : '';
    this.sortBy.emit(this.activeSort);
  }

  render() {
    return (
      <Host>
        <div class="sort-container">
          <div>
            <hotel-filter
              text={SortEnum.alphabetically}
              icon="AB"
              active={this.activeSort === SortEnum.alphabetically}
            ></hotel-filter>
          </div>
          <div>
            <hotel-filter
              text={SortEnum.price}
              icon="£"
              active={this.activeSort === SortEnum.price}
            ></hotel-filter>
          </div>
          <div>
            <hotel-filter
              text={SortEnum.star}
              icon="S"
              active={this.activeSort === SortEnum.star}
            ></hotel-filter>
          </div>
        </div>
      </Host>
    );
  }
}
