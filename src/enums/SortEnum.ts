export const enum SortEnum {
  alphabetically = 'alphabetically',
  price = 'price',
  star = 'star rating'
}
